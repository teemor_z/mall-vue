$(function () {
    app = new Vue({
        el: "#content",
        data: {
            codeToken: "",
            codeImg: "",
            verCode: "",
            account: "",
            password: "",
            disabled: false
        },
        methods: {
            getVerify() {
                _this = this;
                userAxios.get("/user/getVerify")
                    .then(function (response) {
                        result = response.data;
                        if (result.code == 200) {
                            data = result.data;
                            _this.codeToken = data.token;
                            _this.codeImg = data.img;
                        }
                    })
            },
            login() {
                _this = this;
                this.disabled = true;
                var user = {};
                user.verCode = this.verCode;
                user.password = this.password;
                user.account = this.account;
                codeToken = this.codeToken;
                if(/^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/.test(this.account)){
                    user.regType=0;
                }else if(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(this.account)){
                    user.regType=1;
                }else{
                    _this.disabled=false;
                    layer.msg("请输入正确的手机号(邮箱)")
                    return;
                }
                userAxios.post("/user/userLogin",user,{
                    headers:{"VERCODETOKEN":_this.codeToken}
                })
                    .then(function (response) {
                        result = response.data;
                        data = result.data;
                        if (result.code == 200) {
                            var user=data;
                            $.cookie("AUTHTOKEN",data.AUTHTOKEN,{
                                expires:10,
                                path:"/",
                                domanin:"localhost"
                            })
                            if(data.STATUS==1){
                                window.location="/";
                            }else if(data.STATUS==0){
                                window.location="/admin/user/information.html";
                            }
                        }
                        _this.disabled=false;
                        _this.getVerify();
                        layer.msg(result.msg);
                    })
            }
        }
    })
    app.getVerify();
})

