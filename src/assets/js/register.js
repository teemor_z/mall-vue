var myAxios = axios.create({
    baseURL: 'http://localhost:8791/',
    // timeout: 5000,
    headers: {'Content-Type': 'application/json'}
});
app=new Vue({
    el: '#r_form',
    data:{
        verify:'phone',
        typeLabel:'手机',
        account:'15884308429',
        email:"",
        phone:"",
        password:'',
        passwordRepeat:'',
        passwordTip:'',
        regType:'1',
        verCode:'',
        verLabel: '获取验证码',
        totalTime: 5,
        timer:null,
        ds:false,
        verClass:'layui-btn layui-btn-normal',
        subDisabled:false
    },
    methods: {
        verification() {//获取验证码
            if(!/^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/.test(this.phone)&&this.regType==0){
                layer.msg( '请输入正确的手机号');
                return;
            }
            if(!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(this.email)&&this.regType==1){
                layer.msg( '请输入正确的邮箱地址');
               return;
            }
            this.account=this.regType==1?this.email:this.phone;
            var _this=this;
            myAxios.get('getVerCode',{
                params:{
                    account:this.account,
                    regType:this.regType
                }
            }).then(function(response) {
                var data=response.data;
                var code=data.code;
                if (code!=200){
                    layer.msg(data.msg);
                    var hisTime=data.data.hisTime;
                    if (hisTime!=null&&hisTime!=undefined){
                        var nowTime=new Date().getTime();
                        _this.totalTime=Math.ceil(60-((nowTime-hisTime)/1000));
                        _this.$options.methods.daojishi(_this);
                    }

                } else {
                    _this.$options.methods.daojishi(_this);
                    layer.msg(data.msg);
                }
            }).catch(function(){
                layer.msg('出现了不可知问题 QAQ、');
            });
        },//verification()
        daojishi(_this){//验证码获取倒计时
            _this.verLabel = '重新获取('+_this.totalTime+')';
            _this.ds="disabled";
            _this.verClass="layui-btn layui-btn-disabled";
            _this.timer=window.setInterval(()=>{
                _this.totalTime--;
                if(_this.totalTime<=0){
                    _this.verLabel = '获取验证码';
                    _this.totalTime=5;
                    _this.ds=false;
                    _this.verClass='layui-btn layui-btn-normal';
                    clearInterval(_this.timer);
                }else
                    _this.verLabel = '重新获取('+_this.totalTime+')';
            }, 1000)
        },
        clear(){//重置验证码
            this.totalTime=0
            this.verLabel='获取验证码'
            this.ds=false
        },
        verifyPassword(){//校验密码
            p1=this.password;
            p2=this.passwordRepeat;
            if(p1!=p2&&p2!=""){
                this.passwordTip="两次密码不同"
            }else{
                this.passwordTip=""
            }
        },
        saveUser(){
            this.subDisabled=true;
            this.account=this.regType==1?this.email:this.phone;
            account=this.account;
            regType=this.regType;
            verifyT=tool.verifyAccount(account,regType);
            if(!verifyT){
                this.subDisabled=false;
                return;
            }
                
            password=this.password;
            passwordRepeat=this.passwordRepeat;
            verifyT=tool.verifyPassword(password,passwordRepeat);
            if(!verifyT){
                this.subDisabled=false;
                return;
            }
            verCode=this.verCode;
            myAxios.post(
                '/addUsers',
                {
                    "account":account,
                    "password":password,
                    "verCode":verCode,
                    "regType":regType
                }
                )
                .then(function(response){
                    this.subDisabled=false;
                    result=response.data;
                    if(result.code==200){
                        data=result.data;
                        account=data.account;
                        window.location="/regSuccess.html?userId="+account;
                        return;
                    }
                    layer.msg(result.msg);
                })
            var _this=this;
        },
        
    }

});
layui.use(['layer', 'form'], function(){
  });


var tool={
    verifyAccount:function(account,type){
        if(!/^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/.test(account)&&type==0){
            layer.msg( '请输入正确的手机号');
            return false;
        }
        if(!/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(account)&&type==1){
            layer.msg( '请输入正确的邮箱地址');
           return false;
        }
        return true;
    }, 
    verifyPassword:function(p1,p2){
        if(p1==""||p2==""){
            layer.msg( '密码不能为空');
            return false;
        }
        if(p1!=p2&&p2!=""){
            layer.msg( '两次密码必须相同');
            return false;
        }
        return true;
    }
    
}
// layui.use('form', function(){
//     var form = layui.form;

//     //监听提交
//     form.on('submit(formDemo)', function(data){
//         data.field.password=md5(data.field.password);
//         myAxios.post('addUsers',
//             JSON.stringify(data.field)
//         ).then(function(response) {
//             var data=response.data;
//             if (data.code==200){
//                 layer.msg("恭喜你注册成功");
//                 location.href="login.html"
//             }else{
//                 layer.msg(data.msg);
//             }
//         }).catch(function(){
//             console.log('请求失败:');
//         });
//         return false;
//     });
//     form.on('submit(filter)',function(data){

//         return false;
//     });
//     form.verify({
//         username: function(value, item){ //value：表单的值、item：表单的DOM对象
//             if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
//                 return '用户名不能有特殊字符';
//             }
//             if(/(^\_)|(\__)|(\_+$)/.test(value)){
//                 return '用户名首尾不能出现下划线\'_\'';
//             }
//             if(/^\d+\d+\d$/.test(value)){
//                 return '用户名不能全为数字';
//             }
//         }

//         //我们既支持上述函数式的方式，也支持下述数组的形式
//         //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
//         ,pass: [
//             /^[\S]{6,12}$/
//             ,'密码必须6到12位，且不能出现空格'
//         ]
//     });

// });