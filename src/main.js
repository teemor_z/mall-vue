// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import echarts from 'echarts'
import App from './App'
import router from './router'
import layer from 'vue-layer'
import axios from 'axios'
import VueCropper from 'vue-cropper'
import VueResource from 'vue-resource'
import md5 from 'js-md5'


Vue.use(VueResource)
Vue.use(ElementUI)
Vue.use(VueCropper)
Vue.prototype.$echarts = echarts
Vue.prototype.axios = axios
Vue.prototype.$layer = layer(Vue)
Vue.config.productionTip = false
Vue.prototype.$md5 = md5

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App
  },
  template: '<App/>'
})
