import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
export default new Router({
  // mode: 'history',
  routes: [{
    path: '/',
    name: 'home',
    component: resolve => require(['@/views/home'], resolve)
  }, {
    path: '/login',
    name: 'login',
    component: resolve => require(['@/views/mall/login'], resolve)
  }, {
    path: '/register',
    name: 'register',
    component: resolve => require(['@/views/mall/register'], resolve)
  }, {
    path: '/pay',
    name: 'pay',
    component: resolve => require(['@/views/mall/pay'], resolve)
  }, {
    path: '/console/',
    name: 'admin',
    component: resolve => require(['@/views/admin/admin'], resolve)
  }, {
    path: '/search',
    name: 'search',
    component: resolve => require(['@/views/mall/search'], resolve)
  }, {
    path: '/item',
    name: 'item',
    component: resolve => require(['@/views/mall/item'], resolve)
  }, {
    path: '/mall/foot',
    name: 'foot',
    component: resolve => require(['@/views/admin/user/foot'], resolve)
  }, {
    path: '/mall/collect',
    name: 'collect',
    component: resolve => require(['@/views/admin/user/collection'], resolve)
  }, {
    path: '/user/address',
    name: 'address',
    component: resolve => require(['@/views/admin/user/uaddress'], resolve)
  }, {
    path: '/user',
    name: 'user',
    component: resolve => require(['@/views/admin/user/user'], resolve)
  }, {
    path: '/user/security',
    name: 'security',
    component: resolve => require(['@/views/admin/user/security/security'], resolve),
    children: [{
      path: '/user/security/',
      name: 'securityli',
      component: resolve => require(['@/views/admin/user/security/securityli'], resolve)
    },
    {
      path: '/user/security/restpwd',
      name: 'restpwd',
      component: resolve => require(['@/views/admin/user/security/restpwd'], resolve)
    },
    {
      path: '/user/security/paypwd',
      name: 'paypwd',
      component: resolve => require(['@/views/admin/user/security/paypwd'], resolve)
    }, {
      path: '/user/security/bindphone',
      name: 'bindphone',
      component: resolve => require(['@/views/admin/user/security/bindphone'], resolve)
    }, {
      path: '/user/security/idcard',
      name: 'idcard',
      component: resolve => require(['@/views/admin/user/security/idcard'], resolve)
    }, {
      path: '/user/security/bindemail',
      name: 'bindemail',
      component: resolve => require(['@/views/admin/user/security/bindemail'], resolve)
    }
    ]
  }, {
    path: '/shopcart',
    name: 'shopcart',
    component: resolve => require(['@/views/mall/shopcart'], resolve)
  }, {
    path: '/order/confirm',
    name: 'orderconfirm',
    component: resolve => require(['@/views/mall/orderconfirm'], resolve)
  }, {
    path: '/order/list',
    name: 'order',
    component: resolve => require(['@/views/admin/trade/order'], resolve)
  }, {
    path: '/order/info',
    name: 'orderinfo',
    component: resolve => require(['@/views/admin/trade/orderinfo'], resolve)
  }, {
    path: '/pay/success',
    name: 'paysuccess',
    component: resolve => require(['@/views/admin/trade/paysuccess'], resolve)
  }, {
    path: '/pay/success2',
    name: 'paysuccess2',
    component: resolve => require(['@/views/admin/trade/paysuccess2'], resolve)
  }, {
    path: '/person/account',
    name: 'account',
    component: resolve => require(['@/views/admin/person/account.vue'], resolve)
  }, {
    path: '/person/bill',
    name: 'bill',
    component: resolve => require(['@/views/admin/person/bill.vue'], resolve)
  }, {
    path: '/person/bills',
    name: 'bills',
    component: resolve => require(['@/views/admin/person/bills.vue'], resolve)
  }, {
    path: '/person/integral',
    name: 'integral',
    component: resolve => require(['@/views/admin/person/integral.vue'], resolve)
  }, {
    path: '/order/comment',
    name: 'comment',
    component: resolve => require(['@/views/admin/trade/comment'], resolve)
  }, {
    path: '/order/comments',
    name: 'comments',
    component: resolve => require(['@/views/admin/trade/comments'], resolve)
  }, {
    path: '/confirm',
    name: 'confirm',
    component: resolve => require(['@/views/mall/confirm'], resolve)
  }, {
    path: '/messages',
    name: 'messages',
    component: resolve => require(['@/views/admin/user/message'], resolve)
  }, {
    path: '/user2',
    name: 'user2',
    component: resolve => require(['@/views/user2'], resolve)
  }]
})
