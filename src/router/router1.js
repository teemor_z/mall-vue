import Vue from 'vue'
import Router from 'vue-router'
import login from '@/views/mall/login'
import register from '@/views/mall/register'
import admin from '@/views/admin/admin'
import search from '@/views/mall/search'
import item from '@/views/mall/item'
import foot from '@/views/admin/user/foot'
import collect from '@/views/admin/user/collection'
import uaddress from '@/views/admin/user/uaddress'
import user from '@/views/admin/user/user'
import security from '@/views/admin/user/security/security'
import securityli from '@/views/admin/user/security/securityli'
import restpwd from '@/views/admin/user/security/restpwd'
import paypwd from '@/views/admin/user/security/paypwd'
import bindphone from '@/views/admin/user/security/bindphone'
import bindemail from '@/views/admin/user/security/bindemail'
import idcard from '@/views/admin/user/security/idcard'
import pay from '@/views/mall/pay.vue'
import shopcart from '@/views/mall/shopcart'
import order from '@/views/admin/trade/order'
import orderconfirm from '@/views/mall/orderconfirm'
import orderinfo from '@/views/admin/trade/orderinfo'
import account from '@/views/admin/person/account.vue'
import bill from '@/views/admin/person/bill.vue'
import bills from '@/views/admin/person/bills.vue'
import integral from '@/views/admin/person/integral.vue'
import comment from '@/views/admin/trade/comment'
import comments from '@/views/admin/trade/comments'
Vue.use(Router)
export default new Router({
  mode: 'history1',
  routes: [{
    path: '/',
    name: 'home',
    component: resolve => require(['@/views/home'], resolve)
  }, {
    path: '/login',
    name: 'login',
    component: login
  }, {
    path: '/register',
    name: 'register',
    component: register
  }, {
    path: '/pay',
    name: 'pay',
    component: pay
  }, {
    path: '/console/',
    name: 'admin',
    component: admin
  }, {
    path: '/search',
    name: 'search',
    component: search
  }, {
    path: '/item',
    name: 'item',
    component: item
  }, {
    path: '/mall/foot',
    name: 'foot',
    component: foot
  }, {
    path: '/mall/collect',
    name: 'collect',
    component: collect
  }, {
    path: '/user/address',
    name: 'address',
    component: uaddress
  }, {
    path: '/user',
    name: 'user',
    component: user
  }, {
    path: '/user/security',
    name: 'security',
    component: security,
    children: [{
        path: '/user/security/',
        name: 'securityli',
        component: securityli
      },
      {
        path: '/user/security/restpwd',
        name: 'restpwd',
        component: restpwd
      },
      {
        path: '/user/security/paypwd',
        name: 'paypwd',
        component: paypwd
      }, {
        path: '/user/security/bindphone',
        name: 'bindphone',
        component: bindphone
      }, {
        path: '/user/security/idcard',
        name: 'idcard',
        component: idcard
      }, {
        path: '/user/security/bindemail',
        name: 'bindemail',
        component: bindemail
      }
    ]
  }, {
    path: '/shopcart',
    name: 'shopcart',
    component: shopcart
  }, {
    path: '/order/confirm',
    name: 'orderconfirm',
    component: orderconfirm
  }, {
    path: '/order/list',
    name: 'order',
    component: order
  }, {
    path: '/order/info',
    name: 'orderinfo',
    component: orderinfo
  }, {
    path: '/person/account',
    name: 'account',
    component: account
  }, {
    path: '/person/bill',
    name: 'bill',
    component: bill
  }, {
    path: '/person/bills',
    name: 'bills',
    component: bills
  }, {
    path: '/person/integral',
    name: 'integral',
    component: integral
  }, {
    path: '/order/comment',
    name: 'comment',
    component: comment
  }, {
    path: '/order/comments',
    name: 'comments',
    component: comments
  }]
})
